#!/bin/bash
target=../svg-gen-utils
cp -Rv ./src/utils/svg-gen-utils/* $target/src

cd $target
ls
git add .
git commit
npm version patch
npm publish --access public
git push