const txtUtils = require('./txtUtils');
const renderUtils = require('./renderUtils');
const maskUtils = require('./maskUtils');
const etcUtils = require('./etcUtils');
const { pathsFromAbsPts, PathTools } = require('./pathUtils/pathFromAbsPts')
const { pathUtils } = require('./pathUtils/pathUtils');

export default {
    pt: etcUtils.pt,
    text: txtUtils,
    path: {
        default: pathUtils,
        pathUtils,
        pathsFromAbsPts,
        PathTools
    },
    etc: etcUtils,
    mask: maskUtils,
    renderer: renderUtils
};