const { pathsFromAbsPts: pathsFromAbsPtsOrg, PathTools: PathToolsOrg } = require('./pathFromAbsPts')
const { pathUtils: pathUtilsOrg } = require('./pathUtils');
const { parse: parseOrg } = require('./pathParser');


export const pathUtils = pathUtilsOrg;
export const pathsFromAbsPts = pathsFromAbsPtsOrg;
export const PathTools = PathToolsOrg;
export const parse = parseOrg;

export default pathUtils;
