#!/bin/bash
target=../wootra.github.io/
npm run build
cp -Rv ./build/* $target

cd $target
git add .
git commit
git push